/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto1progra2.entities;

/**
 *
 * @author Elizabeth
 */
public class Casilla {
    
    private int fila;
    private int columna;
    private Pieza pieza;
    private COlor color;
    private Coordenada coordenada;
    private boolean ocupada;
    
    public Casilla() {
    }

    public Casilla(int fila, int columna, Pieza pieza, COlor color) {
        this.fila = fila;
        this.columna = columna;
        this.pieza = pieza;
        this.color = color;
    }

    public COlor getColor() {
        return color;
    }

    public int getColumna() {
        return columna;
    }

    public int getFila() {
        return fila;
    }

    public Pieza getPieza() {
        return pieza;
    }

    public void setColor(COlor color) {
        this.color = color;
    }

    public void setColumna(int columna) {
        this.columna = columna;
    }

    public void setPieza(Pieza pieza) {
        this.pieza = pieza;
    }

    public boolean isOcupada() {
        return ocupada;
    }

    public Coordenada getCoordenada() {
        return coordenada;
    }

    public void setCoordenada(Coordenada coordenada) {
        this.coordenada = coordenada;
    }

    public void setFila(int fila) {
        this.fila = fila;
    }

    public void setOcupada(boolean ocupada) {
        this.ocupada = ocupada;
    }

    @Override
    public String toString() {
        return String.format("Coordenada: %s, color: %s, ocupada: %s", coordenada, color.toString(), isOcupada() ? "si" : "no");
    }
    
    
}
