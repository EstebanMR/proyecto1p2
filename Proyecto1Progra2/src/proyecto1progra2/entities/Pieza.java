/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto1progra2.entities;



/**
 *
 * @author Elizabeth
 */
public abstract class Pieza {

    COlor color;
    Casilla casilla;
    boolean Mov;

    public Pieza(COlor  color) {
        this.color = color;
    }

    public boolean ismismocolor(COlor c) {
        if (c == color) {
            return true;
        } else {
            return false;
        }
    }

    public COlor getColor() {
        return color;
    }

    public Casilla getCasilla() {
        return casilla;
    }

    public abstract boolean poderMover();

    public abstract void mover(Casilla casilla);

}
