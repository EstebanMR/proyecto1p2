/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto1progra2.entities;

/**
 *
 * @author muril
 */
public class Tablero {

    private Casilla[][] casillas;

    public Tablero() {
        generar();
    }

    public void generar() {
        casillas = new Casilla[8][8];
        for (int f = 0; f < casillas.length; f++) {
            for (int c = 0; c < casillas[f].length; c++) {

                COlor color = COlor.Blanco;
                if (f % 2 != 0 && c % 2 != 0 || f % 2 == 0 && c % 2 == 0) {
                    color = COlor.Negro;
                } else {
                    color = COlor.Blanco;
                }

                Pieza p = null;

                if (f == 1) {
                    p = new Peon(casillas[f][c], COlor.Negro);
                } else if (f == 6) {
                    p = new Peon(casillas[f][c], COlor.Blanco);
                } else if (f == 0 && (c == 0 || c == 7)) {
                    p = new Torre(casillas[f][c], COlor.Negro);
                } else if (f == 7 && (c == 0 || c == 7)) {
                    p = new Torre(casillas[f][c], COlor.Blanco);
                } else if (f == 0 && (c == 1 || c == 6)) {
                    p = new Caballo(casillas[f][c], COlor.Negro);
                } else if (f == 7 && (c == 1 || c == 6)) {
                    p = new Caballo(casillas[f][c], COlor.Blanco);
                } else if (f == 0 && (c == 2 || c == 5)) {
                    p = new Alfil(casillas[f][c], COlor.Negro);
                } else if (f == 7 && (c == 2 || c == 5)) {
                    p = new Alfil(casillas[f][c], COlor.Blanco);
                } else if (f == 0 && c == 3) {
                    p = new Rey(casillas[f][c], COlor.Negro);
                } else if (f == 7 && c == 3) {
                    p = new Rey(casillas[f][c], COlor.Blanco);
                } else if (f == 0 && c == 4) {
                    p = new Reina(casillas[f][c], COlor.Negro);
                } else if (f == 7 && c == 4) {
                    p = new Reina(casillas[f][c], COlor.Blanco);
                }

                Coordenada coor = null;

                switch (f) {
                    case 0:
                        coor = new Coordenada("A", c + 1);
                        break;
                    case 1:
                        coor = new Coordenada("B", c + 1);
                        break;
                    case 2:
                        coor = new Coordenada("C", c + 1);
                        break;
                    case 3:
                        coor = new Coordenada("D", c + 1);
                        break;
                    case 4:
                        coor = new Coordenada("E", c + 1);
                        break;
                    case 5:
                        coor = new Coordenada("F", c + 1);
                        break;
                    case 6:
                        coor = new Coordenada("G", c + 1);
                        break;
                    case 7:
                        coor = new Coordenada("H", c + 1);
                        break;
                    default:
                        break;
                }

                casillas[f][c] = new Casilla();
                casillas[f][c].setColor(color);
                casillas[f][c].setPieza(p);
                casillas[f][c].setFila(f);
                casillas[f][c].setColumna(c);
                casillas[f][c].setCoordenada(coor);
                if (p == null) {
                    casillas[f][c].setOcupada(false);
                } else {
                    casillas[f][c].setOcupada(true);
                }
            }
        }
    }

    public Casilla[][] getCasillas() {
        return casillas;
    }

    public void setCasillas(Casilla[][] casillas) {
        this.casillas = casillas;
    }

}
