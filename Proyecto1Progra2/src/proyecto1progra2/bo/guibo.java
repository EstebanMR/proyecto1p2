/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto1progra2.bo;

import proyecto1progra2.entities.Alfil;
import proyecto1progra2.entities.COlor;
import proyecto1progra2.entities.Caballo;
import proyecto1progra2.entities.Casilla;
import proyecto1progra2.entities.Peon;
import proyecto1progra2.entities.Reina;
import proyecto1progra2.entities.Rey;
import proyecto1progra2.entities.Torre;

/**
 *
 * @author muril
 */
public class guibo {

    public String setup(Casilla casilla) {
        String pi = "";
        if (casilla.getPieza() instanceof Peon) {
            pi += "peon-";
            if (casilla.getPieza().getColor() == proyecto1progra2.entities.COlor.Blanco) {
                pi += "blanca";
            } else if (casilla.getPieza().getColor() == proyecto1progra2.entities.COlor.Negro) {
                pi += "negro";
            }
        } else if (casilla.getPieza() instanceof Torre) {
            pi += "torre-";
            if (casilla.getPieza().getColor() == proyecto1progra2.entities.COlor.Blanco) {
                pi += "blanca";
            } else if (casilla.getPieza().getColor() == proyecto1progra2.entities.COlor.Negro) {
                pi += "negro";
            }

        } else if (casilla.getPieza() instanceof Caballo) {
            pi += "caballo-";
            if (casilla.getPieza().getColor() == proyecto1progra2.entities.COlor.Blanco) {
                pi += "blanca";
            } else if (casilla.getPieza().getColor() == proyecto1progra2.entities.COlor.Negro) {
                pi += "negro";
            }

        } else if (casilla.getPieza() instanceof Alfil) {
            pi += "alfil-";
            if (casilla.getPieza().getColor() == proyecto1progra2.entities.COlor.Blanco) {
                pi += "blanca";
            } else if (casilla.getPieza().getColor() == proyecto1progra2.entities.COlor.Negro) {
                pi += "negro";
            }

        } else if (casilla.getPieza() instanceof Rey) {
            pi += "rey-";
            if (casilla.getPieza().getColor() == proyecto1progra2.entities.COlor.Blanco) {
                pi += "blanca";
            } else if (casilla.getPieza().getColor() == proyecto1progra2.entities.COlor.Negro) {
                pi += "negro";
            }

        } else if (casilla.getPieza() instanceof Reina) {
            pi += "reina-";
            if (casilla.getPieza().getColor() == proyecto1progra2.entities.COlor.Blanco) {
                pi += "blanca";
            } else if (casilla.getPieza().getColor() == proyecto1progra2.entities.COlor.Negro) {
                pi += "negro";
            }

        } else {
            pi = null;
        }
        return pi;
    }

    public int contarFichasN(Casilla[][] casillas) {
        int pb = 0;

        for (int i = 0; i < casillas.length; i++) {
            for (int j = 0; j < casillas[i].length; j++) {
                if (casillas[i][j].getPieza().getColor() == proyecto1progra2.entities.COlor.Negro) {
                    pb++;
                }
            }
        }
        return pb;
    }

    public int contarFichasB(Casilla[][] casillas) {
        int pb = 0;

        for (int i = 0; i < casillas.length; i++) {
            for (int j = 0; j < casillas[i].length; j++) {
                if (casillas[i][j].getPieza().getColor() == proyecto1progra2.entities.COlor.Blanco) {
                    pb++;
                }
            }
        }
        return pb;
    }

    public COlor valColor(String c, Casilla[][] casillas) {
        COlor c1 = null;
        for (int i = 0; i < casillas.length; i++) {
            for (int j = 0; j < casillas[i].length; j++) {
                String coord = casillas[i][j].getCoordenada().getLetra() + "," + casillas[i][j].getCoordenada().getNumero();
                if (coord.equals(c)) {
                    c1 = casillas[i][j].getPieza().getColor();
                }
            }
        }
        return c1;
    }

    public Casilla getcasilla(String c, Casilla[][] casillas) {
        Casilla cas = null;
        for (int i = 0; i < casillas.length; i++) {
            for (int j = 0; j < casillas[i].length; j++) {
                String coord = casillas[i][j].getCoordenada().getLetra() + "," + casillas[i][j].getCoordenada().getNumero();
                if (coord.equals(c)) {
                    cas = casillas[i][j];
                }
            }
        }
        return cas;
    }

    public String getpieza(String nombre) {
        String[] parts = nombre.split("-");
        String part1 = parts[0]; 
        return part1;
    }

}
